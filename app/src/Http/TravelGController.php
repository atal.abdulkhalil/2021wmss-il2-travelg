<?php


namespace Http;

use Services\DatabaseConnector;

class TravelGController
{
    protected \Doctrine\DBAL\Connection $db;
    protected \Twig\Environment $twig;

    public function __construct()
    {
        $loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../../resources/templates');
        $this->twig = new \Twig\Environment($loader);

        $this->db = DatabaseConnector::getConnection();
    }

    public function overview()
    {

        $travelgrams = $this->db->fetchAllAssociative('SELECT travelgrams.id, travelgrams.title, travelgrams.description, DATE_FORMAT(travelgrams.added_on, "%d-%m-%y") AS added_on, authors.firstname FROM travelgrams LEFT JOIN authors on travelgrams.author_id = authors.id ORDER BY travelgrams.added_on DESC LIMIT 2', []);

        echo $this->twig->render('pages/index.twig', [
            'travelgrams' => $travelgrams

        ]);
    }

    public function search()
    {
        $tags = $this->db->fetchAllAssociative('SELECT * FROM `tags` order by title', []);



        echo $this->twig->render('pages/search.twig', [
            'tags' => $tags

        ]);
    }

    public function showSearch($id)
    {
        $tags = $this->db->fetchAllAssociative('SELECT * FROM `tags`', []);
        $travelgrams = $this->db->fetchAllAssociative('SELECT travelgrams.id, travelgrams.title, travelgrams.description, DATE_FORMAT(travelgrams.added_on, "%d-%m-%y") AS added_on, authors.firstname FROM travelgrams LEFT JOIN authors on travelgrams.author_id = authors.id JOIN travelgrams_to_tags on travelgrams.id = travelgrams_to_tags.travelgram_id where travelgrams_to_tags.tag_id = ? ORDER BY travelgrams.added_on DESC', [$id]);

        if ($travelgrams == false){
            header('Location: /search');
            exit();
        }


        echo $this->twig->render('pages/search.twig', [
            'tags' => $tags,
            'travelgrams' => $travelgrams

        ]);
    }

}