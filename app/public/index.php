<?php

require __DIR__ . '/../vendor/autoload.php';
$router = new \Bramus\Router\Router();

// add your routes and run!
$router->setNamespace('\Http');

$router->before('GET|POST', '/.*', function () {
    session_start();

});
/*
$router->before('GET|POST', '/books.*', function () {
    if (! isset($_SESSION['user'])) {
        header('Location: /');
        exit();
    }
});
*/
$router->get('/', 'TravelGController@overview');
$router->get('/search', 'TravelGController@search');
$router->get('/search/(\d+)', 'TravelGController@showSearch');


$router->run();