SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

DROP DATABASE IF EXISTS `2017travelgram`;
CREATE DATABASE `2017travelgram`
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_unicode_ci;
  
USE `2017travelgram`;


DROP TABLE IF EXISTS `authors`;

CREATE TABLE `authors` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `authors` (`id`, `firstname`, `lastname`, `email`, `password`) VALUES
(1, 'Kevin', 'Picalausa', 'kevin.picalausa@odisee.be', '$2y$10$ERrAdBi/yPrdwAzgHOx1ROSZzt1U03wubDGBZu45oWpDRCnO0Frf2'),
(2, 'Joris', 'Maervoet', 'joris.maervoet@odisee.be', '$2y$10$ERrAdBi/yPrdwAzgHOx1ROSZzt1U03wubDGBZu45oWpDRCnO0Frf2'),
(3, 'Pierke', 'Pierlala', 'pierke.pierlala@stadgent.be', '$2y$10$ERrAdBi/yPrdwAzgHOx1ROSZzt1U03wubDGBZu45oWpDRCnO0Frf2');




DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `tags` (`id`, `title`) VALUES
(1, 'Ierland'),
(2, 'bergen'),
(3, 'Italië'),
(4, 'Kroatië'),
(5, 'zee en strand'),
(6, 'Romeins'),
(7, 'UNESCO werelderfgoed'),
(8, 'Tsjechië'),
(9, 'België'),
(10, 'kunst'),
(11, 'Nederland'),
(12, 'kerken'),
(13, 'Spanje'),
(14, 'Frankrijk'),
(15, 'Arabisch en Moors'),
(16, 'Corsica');


DROP TABLE IF EXISTS `travelgrams`;
CREATE TABLE `travelgrams` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(11) NOT NULL,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




INSERT INTO `travelgrams` (`id`, `title`, `description`, `author_id`, `added_on`) VALUES
(1, 'Feel the breeze', 'Een prachtige wandeltocht in de Wicklow Mountains in de buurt van Dublin.', 2, '2006-11-01 14:51:20'),
(11, 'Carnevale', 'Een kleurencarnaval in een Venetiaans winkeltje met maskers.', 2, '2007-07-31 11:11:20'),
(12, 'Rust in Rovinj', 'Binnenplein in het centrumvan het pittoreske Rovinj, net voorbij de Limski-zeearm.', 2, '2007-08-04 13:18:18'),
(13, 'Romeinen in Kroatië', 'Het amfitheater van Pula.', 2, '2007-08-05 14:15:20'),
(14, 'Amfora''s in het amfitheater', 'Te bezichtigen in de kelders van het amfitheater van Pula.', 2, '2007-08-05 14:57:45'),
(15, 'Vesting van Nehaj', 'Zicht vanop de vesting van Nehaj in Senj met o.a. het eiland Krk.', 2, '2007-08-06 19:19:19'),
(16, 'Plitvice: 50 tinten groen en blauw', 'De meren van Plitvice: prachtig UNESCO-werelderfgoed.', 2, '2007-08-07 16:29:16'),
(20, 'De markt van Telč', '15de- en 16de eeuwse huizen op het dorpsplein van Telč in Moravië.', 2, '2008-07-26 19:23:58'),
(25, 'Olifanten op ''t Eilandje', 'Versierde olifanten overal te zien doorheen Antwerpen', 2, '2008-09-14 15:58:58'),
(26, 'Nou... doe maar een koffie in de kerk', 'Wist je dat er een kerk in Maastricht volledig werd omgebouwd tot een boekenwinkel met koffiebar?', 2, '2008-10-19 14:57:14'),
(30, 'Op stap in Segovia', 'Op verkenning doorheen Segovia (Castilië en León).', 2, '2008-12-12 18:09:01'),
(33, 'Zo dul als een griet', 'Geef mij maar Gent. Groetjes, Pierke.', 3, '2009-01-01 07:45:00'),
(35, 'Mosselkweek', 'Foto op het strand van Cap-Griz-Nez.', 2, '2009-03-15 09:53:00'),
(41, 'Moorse geheimen in Granada', 'Eén van de vele magistrale koepels van het Alhambra in Granada.', 2, '2009-04-06 18:20:00'),
(50, 'La más linda de todas', 'Op conferentie naar Salamanca? Zeker doen. Dit is een foto van de Plaza Mayor van Salamanca, officieus bekend als de knapste van Spanje.', 2, '2009-06-12 11:28:00'),
(55, 'Is het een schip?', 'Krijtrotsformaties aan een strandje in de buurt van Bonifacio. Dit is de meest zuidelijke stad van Corsica, volledig gelegen op krijtrotsen.', 2, '2009-09-20 11:33:30')

;

DROP TABLE IF EXISTS `travelgrams_to_tags`;
CREATE TABLE `travelgrams_to_tags` (
  `travelgram_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `travelgrams_to_tags` (`travelgram_id`, `tag_id`) VALUES
(1,1),
(1,2),
(11,3),
(12,4),
(12,5),
(13,4),
(13,6),
(14,4),
(14,6),
(15,4),
(15,5),
(16,4),
(16,7),
(20,7),
(20,8),
(25,9),
(25,10),
(26,11),
(26,12),
(30,13),
(30,12),
(35,14),
(35,5),
(41,7),
(41,13),
(41,10),
(41,15),
(50,13),
(50,7),
(55,14),
(55,16),
(55,5),
(33,9)
;


ALTER TABLE `authors`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `tags`
 ADD PRIMARY KEY (`id`);
 
ALTER TABLE `travelgrams`
 ADD PRIMARY KEY (`id`);
 
ALTER TABLE `travelgrams_to_tags`
 ADD PRIMARY KEY (`travelgram_id`, `tag_id`);

 
 ALTER TABLE `authors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=100;

ALTER TABLE `tags`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;

ALTER TABLE `travelgrams`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;

ALTER TABLE `travelgrams` ADD CONSTRAINT fk_author_id FOREIGN KEY (author_id) REFERENCES authors(id);

ALTER TABLE `travelgrams_to_tags` ADD CONSTRAINT fk_travelgram_id FOREIGN KEY (travelgram_id) REFERENCES travelgrams(id);
ALTER TABLE `travelgrams_to_tags` ADD CONSTRAINT fk_tag_id FOREIGN KEY (tag_id) REFERENCES tags(id);